<?php

require_once 'vendor/autoload.php';

use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Drivers\DriverManager;

$config = [
    'facebook' => [
                    'token' => 'EAAFs2rGYyy0BABEqghpTJcjXVZAi4N7WulpRhsfoa7en83QVOw0jdTU6CBnfPk0y4fzNxTnDhmBkfIM8YxlSDsI4x3y1cVxDrAqYCBzzcmcCDZBwKNs7J4AZCWWXOHTV5CjZCVBUPM53YJd9nJ486Fte3cMEZCB5iDZCDDZBRCbMo7P8t7yz0GlZBaMoZCgcQBdwZD',
                    'app_secret' => 'bf259444444e16f63a8237e8e607c370',
                    'verification'=>'MY_SECRET_VERIFICATION_TOKEN',
  ]
];

// Load the driver(s) you want to use
DriverManager::loadDriver(\BotMan\Drivers\Facebook\FacebookDriver::class);

// Create an instance
$botman = BotManFactory::create($config);

// Give the bot something to listen for.
$botman->hears('hello', function (BotMan $bot) {
    $bot->reply('Hello yourself.');
});

// Start listening
$botman->listen();